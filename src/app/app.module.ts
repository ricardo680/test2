import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { IonicStorageModule } from '@ionic/storage'


import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http'



import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';

import { ResultModalPage } from '../pages/result-modal/result-modal'


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


//import { OrderByPipe } from '../pipes/order-by/order-by'
import { PipesModule } from '../pipes/pipes.module'



import { AuthProvider } from '../providers/requests/auth'
import { PersonProvider } from '../providers/requests/person'
import { ResultProvider } from '../providers/requests/result'
import { StorageProvider } from '../providers/storage'
import { TokensProvider } from '../providers/tokens'

import { TokenInterceptor } from '../providers/interceptors/token-interceptor'




import 'rxjs/add/operator/catch'
import 'rxjs/add/operator/do'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/retry'
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/timeout';




@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,

    ResultModalPage,

    //OrderByPipe,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,

    IonicStorageModule.forRoot(),

    PipesModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,

    ResultModalPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },

    //OrderByPipe,

    AuthProvider,
    PersonProvider,
    ResultProvider,
    StorageProvider,
    TokensProvider,

    TokenInterceptor
  ]
})
export class AppModule {}
