import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Storage } from '@ionic/storage'

import { StorageProvider } from '../../providers/storage'
import { LoginPage } from '../login/login'


@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storageProvider: StorageProvider, private storage: Storage) {
      this.storageProvider.personsDb.clear();
      this.storage.clear()

  }


  ionViewDidLoad(){
    var self = this
    setTimeout(function(){
      self.navCtrl.setRoot(LoginPage)
    },250)
  }
}
