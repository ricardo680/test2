import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ResultModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-result-modal',
  templateUrl: 'result-modal.html',
})
export class ResultModalPage {


  message: any
  next_steps: any
  error: any
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController) {
    //console.log('UserId', navParams.get('userId'));
    console.log('message', navParams.get('message'));
    this.message = navParams.get('message')
    console.log('next_steps', navParams.get('next_steps'));
    this.next_steps = navParams.get('next_steps')


    console.log('error', navParams.get('error'));
    this.error = navParams.get('error')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultModalPage');
  }

  dismiss(){
    this.viewCtrl.dismiss()
  }
}
