import { Component } from '@angular/core'
import { NavController, NavParams } from 'ionic-angular'
import { FormGroup, Validators, FormBuilder } from '@angular/forms'
import { Storage } from '@ionic/storage'
import { SplashScreen } from '@ionic-native/splash-screen'
import { Platform } from 'ionic-angular'
import { AlertController } from 'ionic-angular'

import { AuthProvider } from '../../providers/requests/auth'
import { HomePage } from '../home/home'
import { TokensProvider } from '../../providers/tokens'
import { ValidatorProvider } from '../../providers/validator';



@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  myForm: FormGroup

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private auth: AuthProvider, fb: FormBuilder, private alertCtrl: AlertController,
    private storage: Storage, private tokens: TokensProvider,
    private splashScreen: SplashScreen, public platform: Platform) {
    this.myForm = fb.group({
      email: ['', [Validators.required, ValidatorProvider.emailValidator]],
      remember: ['true']
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage')

    // load remeber values in form
    this.storage.get('login-form').then((val) => {
      if (val) {
        for (var id in val) {
          this.myForm.controls[id].setValue(val[id])
        }

        this.storage.get('ob-api-token').then((val) => {
          if (val) {
            this.tokens.obApiToken = val
            setTimeout(() => {
              this.navCtrl.setRoot(HomePage)
            }, 100)
          }
        })
      }
    })


    this.platform.ready().then(() => {
      // do whatever you need to do here.
      setTimeout(() => {
        this.splashScreen.hide()
      }, 100)
    })

  }

  login() {
    if (!this.myForm.valid){
      console.log('INVALID', this.myForm.value)
      return
    }

    var values = Object.assign({}, this.myForm.value)

    if (values) {
      this.storage.set('login-form', this.myForm.value)
      this.storage.get('login-form').then((val) => {
        console.log(val)
      })
    }

    delete values.remember
    this.auth.login(this.myForm.value).subscribe(data => {
      if (data) {
        console.log('login ',data)
        if (!this.myForm.value.remember) this.storage.remove('login-form')
        this.storage.set('ob-api-token', data.token)
        this.tokens.obApiToken = data.token
        //email, message, token
        this.navCtrl.setRoot(HomePage)
        return
      }
      this.showError()
    }, err => {
      this.showError()
      console.error(err)
    })
  }

  private showError() {
    const alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: 'Revise su información y vuelva intentarlo',
      buttons: ['Aceptar']
    })
    alert.present()
  }
}
