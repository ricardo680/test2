import { Component } from '@angular/core';
import { NavController, LoadingController, ModalController } from 'ionic-angular';

import { PersonProvider } from '../../providers/requests/person'
import { StorageProvider } from '../../providers/storage'

import { LargestContinguousIncreasingPipe } from '../../pipes/largest-continguous-increasing/largest-continguous-increasing'

import { ResultProvider } from '../../providers/requests/result'


import { ResultModalPage } from '../../pages/result-modal/result-modal'


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  personsShow: any = []
  persons: any = []
  nPersons = 0

  loadingPersons: any
  json: any = {}

  constructor(public navCtrl: NavController, private person: PersonProvider, private result: ResultProvider,
  private storage: StorageProvider, public loadingCtrl: LoadingController, public modalCtrl: ModalController
  ) { }

  ionViewDidLoad(){
    //this.local()/// quit??
  }





  getPersons() {
    this.loadingPersons = this.loadingCtrl.create({
      content: 'Geting Persons...'
    });
    this.loadingPersons.present();

    this.getNextPerson()
  }

  getNextPerson() {
    var self = this
    this.person.read().subscribe(data => {
      self.nPersons++
      if (data) {
        var idS = this.zfill(self.nPersons, 10) // generate ID
        console.log(idS, data)
        this.storage.personsDb.set(idS, data);  //save local
        this.getNextPerson()
        return
      }
    }, err => {
      if(err.status==404) {
        self.loadingPersons.dismiss();
        self.local()
        alert ('finished');
        return
      }

      console.error(err)
    })
  }





  local() {
    this.loadingPersons = []
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();

    this.storage.personsDb.forEach((data, id, itN) => {
      this.persons.push(data)               // show in page
    })
    .then((val) => {
      loading.dismiss();
      console.log('done');
      this.filter()
    });
  }






  filter() {
    let loading = this.loadingCtrl.create({
      content: 'Process algoritms ...'
    });
    loading.present();


     this.personsShow = new LargestContinguousIncreasingPipe().transform(this.persons, '')

     /// averange
     var sum = 0;
     for( var i = 0; i < this.personsShow.length; i++ ) {
       sum += parseInt( this.personsShow[i].age, 10 ); //don't forget to add the base
     }
     var avg = sum/this.personsShow.length;
     console.log("AVG: ", avg)


     // order by names and get first letter lastname
     var names = []
     for( var i = 0; i < this.personsShow.length; i++ ) {
       names.push(this.personsShow[i].name)
     }
     names.sort()

     var firstLetters = ''
     for( var i = 0; i < names.length; i++ ) {
       firstLetters += names[i].split(" ")[1][0]
     }
     console.log("firstLetters: ", firstLetters)

     // json
     this.json = {
       age: Math.trunc(avg),
       payload: firstLetters,
       code: "DQoNCg0KaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7DQppbXBvcnQgeyBOYXZDb250cm9sbGVyLCBMb2FkaW5nQ29udHJvbGxlciB9IGZyb20gJ2lvbmljLWFuZ3VsYXInOw0KDQppbXBvcnQgeyBQZXJzb25Qcm92aWRlciB9IGZyb20gJy4uLy4uL3Byb3ZpZGVycy9yZXF1ZXN0cy9wZXJzb24nDQppbXBvcnQgeyBTdG9yYWdlUHJvdmlkZXIgfSBmcm9tICcuLi8uLi9wcm92aWRlcnMvc3RvcmFnZScNCg0KaW1wb3J0IHsgTGFyZ2VzdENvbnRpbmd1b3VzSW5jcmVhc2luZ1BpcGUgfSBmcm9tICcuLi8uLi9waXBlcy9sYXJnZXN0LWNvbnRpbmd1b3VzLWluY3JlYXNpbmcvbGFyZ2VzdC1jb250aW5ndW91cy1pbmNyZWFzaW5nJw0KDQoNCkBDb21wb25lbnQoew0KICBzZWxlY3RvcjogJ3BhZ2UtaG9tZScsDQogIHRlbXBsYXRlVXJsOiAnaG9tZS5odG1sJw0KfSkNCmV4cG9ydCBjbGFzcyBIb21lUGFnZSB7DQoNCiAgcGVyc29uc1Nob3c6IGFueSA9IFtdDQogIHBlcnNvbnM6IGFueSA9IFtdDQoNCiAgY29uc3RydWN0b3IocHVibGljIG5hdkN0cmw6IE5hdkNvbnRyb2xsZXIsIHByaXZhdGUgcGVyc29uOiBQZXJzb25Qcm92aWRlciwNCiAgcHJpdmF0ZSBzdG9yYWdlOiBTdG9yYWdlUHJvdmlkZXIsIHB1YmxpYyBsb2FkaW5nQ3RybDogTG9hZGluZ0NvbnRyb2xsZXIsDQogICkgew0KICB9DQoNCiAgaW9uVmlld0RpZExvYWQoKXsNCiAgICB0aGlzLmxvY2FsKCkvLy8gcXVpdD8/DQogIH0NCg0KDQogIGxvY2FsKCkgew0KICAgIGxldCBsb2FkaW5nID0gdGhpcy5sb2FkaW5nQ3RybC5jcmVhdGUoew0KICAgICAgY29udGVudDogJ1BsZWFzZSB3YWl0Li4uJw0KICAgIH0pOw0KICAgIGxvYWRpbmcucHJlc2VudCgpOw0KDQogICAgdGhpcy5zdG9yYWdlLnBlcnNvbnNEYi5mb3JFYWNoKChkYXRhLCBpZCwgaXROKSA9PiB7DQogICAgICB0aGlzLnBlcnNvbnMucHVzaChkYXRhKSAgICAgICAgICAgICAgIC8vIHNob3cgaW4gcGFnZQ0KICAgIH0pDQogICAgLnRoZW4oKHZhbCkgPT4gew0KICAgICAgbG9hZGluZy5kaXNtaXNzKCk7DQogICAgICBjb25zb2xlLmxvZygnZG9uZScpOw0KICAgICAgdGhpcy5maWx0ZXIoKQ0KICAgIH0pOw0KDQogIH0NCg0KDQogIGdldFBlcnNvbigpIHsNCiAgICB0aGlzLnBlcnNvbi5yZWFkKCkuc3Vic2NyaWJlKGRhdGEgPT4gew0KICAgICAgaWYgKGRhdGEpIHsNCiAgICAgICAgdGhpcy5zdG9yYWdlLnBlcnNvbnNEYi5zZXQoIGRhdGEucGVyc29uSWQsIGRhdGEpOyAgLy9zYXZlIGxvY2FsDQogICAgICAgIHRoaXMuZ2V0UGVyc29uKCkNCiAgICAgICAgcmV0dXJuDQogICAgICB9DQogICAgfSwgZXJyID0+IHsNCiAgICAgIGlmKGVyci5zdGF0dXM9PTQwNCkgew0KICAgICAgICBhbGVydCAoJ29rJyk7DQogICAgICB9DQogICAgICBjb25zb2xlLmVycm9yKGVycikNCg0KICAgIH0pDQogIH0NCg0KICBmaWx0ZXIoKSB7DQogICAgIHRoaXMucGVyc29uc1Nob3cgPSBuZXcgTGFyZ2VzdENvbnRpbmd1b3VzSW5jcmVhc2luZ1BpcGUoKS50cmFuc2Zvcm0odGhpcy5wZXJzb25zLCAnJykNCg0KDQogICAgIC8vLyBhdmVyYW5nZQ0KICAgICB2YXIgc3VtID0gMDsNCiAgICAgZm9yKCB2YXIgaSA9IDA7IGkgPCB0aGlzLnBlcnNvbnNTaG93Lmxlbmd0aDsgaSsrICkgew0KICAgICAgIHN1bSArPSBwYXJzZUludCggdGhpcy5wZXJzb25zU2hvd1tpXS5hZ2UsIDEwICk7IC8vZG9uJ3QgZm9yZ2V0IHRvIGFkZCB0aGUgYmFzZQ0KICAgICB9DQogICAgIHZhciBhdmcgPSBzdW0vdGhpcy5wZXJzb25zU2hvdy5sZW5ndGg7DQogICAgIGNvbnNvbGUubG9nKCJBVkc6ICIsIGF2ZykNCg0KDQogICAgIC8vIG9yZGVyIGJ5IG5hbWVzIGFuZCBnZXQgZmlyc3QgbGV0dGVyIGxhc3RuYW1lDQogICAgIHZhciBuYW1lcyA9IFtdDQogICAgIGZvciggdmFyIGkgPSAwOyBpIDwgdGhpcy5wZXJzb25zU2hvdy5sZW5ndGg7IGkrKyApIHsNCiAgICAgICBuYW1lcy5wdXNoKHRoaXMucGVyc29uc1Nob3dbaV0ubmFtZSkNCiAgICAgfQ0KICAgICBuYW1lcy5zb3J0KCkNCg0KICAgICB2YXIgZmlyc3RMZXR0ZXJzID0gJycNCiAgICAgZm9yKCB2YXIgaSA9IDA7IGkgPCBuYW1lcy5sZW5ndGg7IGkrKyApIHsNCiAgICAgICBmaXJzdExldHRlcnMgKz0gbmFtZXNbaV0uc3BsaXQoIiAiKVsxXVswXQ0KICAgICB9DQogICAgIGNvbnNvbGUubG9nKCJmaXJzdExldHRlcnM6ICIsIGZpcnN0TGV0dGVycykNCg0KDQogICAgIC8vIGpzb24NCiAgICAgdmFyIGpzb24gPSB7DQogICAgICAgYWdlOiBhdmcsDQogICAgICAgcGF5bG9hZDogZmlyc3RMZXR0ZXJzLA0KICAgICAgIGNvZGU6ICJiYXNlNjQiDQogICAgIH0NCiAgfQ0KDQp9DQoNCg0KDQoNCg0KDQoNCg0KDQoNCg0KDQppbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7DQppbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7DQoNCkBQaXBlKHsNCiAgbmFtZTogJ2xhcmdlc3RDb250aW5ndW91c0luY3JlYXNpbmcnLA0KfSkNCmV4cG9ydCBjbGFzcyBMYXJnZXN0Q29udGluZ3VvdXNJbmNyZWFzaW5nUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0gew0KICB0cmFuc2Zvcm0odmFsdWVzOiBhbnksIC4uLmFyZ3MpIHsNCiAgICB2YXIgbGFyZ2VzdEFycmF5ID0gW10NCiAgICB2YXIgY3VycmVudEFycmF5ID0gW10NCg0KICAgIHZhbHVlcy5mb3JFYWNoKGZ1bmN0aW9uKGN1cnJlbnQsIGlkKSB7DQogICAgICBpZiAoaWQgPT0gIjAiKSB7DQogICAgICAgIGN1cnJlbnRBcnJheS5wdXNoKGN1cnJlbnQpDQogICAgICAgIHJldHVybg0KICAgICAgfQ0KDQogICAgICBsZXQgbGFzdCA9IHZhbHVlc1tpZC0xXQ0KICAgICAgaWYgKGxhc3QuYWdlID49IGN1cnJlbnQuYWdlKSB7DQogICAgICAgIGlmIChjdXJyZW50QXJyYXkubGVuZ3RoID4gbGFyZ2VzdEFycmF5Lmxlbmd0aCkgew0KICAgICAgICAgIGxhcmdlc3RBcnJheSA9IGN1cnJlbnRBcnJheQ0KICAgICAgICB9DQogICAgICAgIGN1cnJlbnRBcnJheSA9IFtdDQogICAgICB9DQoNCiAgICAgIGN1cnJlbnRBcnJheS5wdXNoKGN1cnJlbnQpDQogICAgfSkNCg0KICAgIHJldHVybiBsYXJnZXN0QXJyYXkNCiAgfQ0KfQ0KDQoNCg0KDQoNCg0KDQoNCmltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnDQppbXBvcnQgeyBOYXZDb250cm9sbGVyLCBOYXZQYXJhbXMgfSBmcm9tICdpb25pYy1hbmd1bGFyJw0KaW1wb3J0IHsgRm9ybUdyb3VwLCBWYWxpZGF0b3JzLCBGb3JtQnVpbGRlciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJw0KaW1wb3J0IHsgU3RvcmFnZSB9IGZyb20gJ0Bpb25pYy9zdG9yYWdlJw0KaW1wb3J0IHsgU3BsYXNoU2NyZWVuIH0gZnJvbSAnQGlvbmljLW5hdGl2ZS9zcGxhc2gtc2NyZWVuJw0KaW1wb3J0IHsgUGxhdGZvcm0gfSBmcm9tICdpb25pYy1hbmd1bGFyJw0KaW1wb3J0IHsgQWxlcnRDb250cm9sbGVyIH0gZnJvbSAnaW9uaWMtYW5ndWxhcicNCg0KaW1wb3J0IHsgQXV0aFByb3ZpZGVyIH0gZnJvbSAnLi4vLi4vcHJvdmlkZXJzL3JlcXVlc3RzL2F1dGgnDQppbXBvcnQgeyBIb21lUGFnZSB9IGZyb20gJy4uL2hvbWUvaG9tZScNCmltcG9ydCB7IFRva2Vuc1Byb3ZpZGVyIH0gZnJvbSAnLi4vLi4vcHJvdmlkZXJzL3Rva2VucycNCmltcG9ydCB7IFZhbGlkYXRvclByb3ZpZGVyIH0gZnJvbSAnLi4vLi4vcHJvdmlkZXJzL3ZhbGlkYXRvcic7DQoNCg0KDQpAQ29tcG9uZW50KHsNCiAgc2VsZWN0b3I6ICdwYWdlLWxvZ2luJywNCiAgdGVtcGxhdGVVcmw6ICdsb2dpbi5odG1sJywNCn0pDQpleHBvcnQgY2xhc3MgTG9naW5QYWdlIHsNCg0KICBteUZvcm06IEZvcm1Hcm91cA0KDQogIGNvbnN0cnVjdG9yKHB1YmxpYyBuYXZDdHJsOiBOYXZDb250cm9sbGVyLCBwdWJsaWMgbmF2UGFyYW1zOiBOYXZQYXJhbXMsDQogICAgcHJpdmF0ZSBhdXRoOiBBdXRoUHJvdmlkZXIsIGZiOiBGb3JtQnVpbGRlciwgcHJpdmF0ZSBhbGVydEN0cmw6IEFsZXJ0Q29udHJvbGxlciwNCiAgICBwcml2YXRlIHN0b3JhZ2U6IFN0b3JhZ2UsIHByaXZhdGUgdG9rZW5zOiBUb2tlbnNQcm92aWRlciwNCiAgICBwcml2YXRlIHNwbGFzaFNjcmVlbjogU3BsYXNoU2NyZWVuLCBwdWJsaWMgcGxhdGZvcm06IFBsYXRmb3JtKSB7DQogICAgdGhpcy5teUZvcm0gPSBmYi5ncm91cCh7DQogICAgICBlbWFpbDogWycnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9yUHJvdmlkZXIuZW1haWxWYWxpZGF0b3JdXSwNCiAgICAgIHJlbWVtYmVyOiBbJ3RydWUnXQ0KICAgIH0pDQogIH0NCg0KICBpb25WaWV3RGlkTG9hZCgpIHsNCiAgICBjb25zb2xlLmxvZygnaW9uVmlld0RpZExvYWQgTG9naW5QYWdlJykNCg0KICAgIC8vIGxvYWQgcmVtZWJlciB2YWx1ZXMgaW4gZm9ybQ0KICAgIHRoaXMuc3RvcmFnZS5nZXQoJ2xvZ2luLWZvcm0nKS50aGVuKCh2YWwpID0+IHsNCiAgICAgIGlmICh2YWwpIHsNCiAgICAgICAgZm9yICh2YXIgaWQgaW4gdmFsKSB7DQogICAgICAgICAgdGhpcy5teUZvcm0uY29udHJvbHNbaWRdLnNldFZhbHVlKHZhbFtpZF0pDQogICAgICAgIH0NCiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7DQogICAgICAgICAgdGhpcy5sb2dpbigpDQogICAgICAgIH0sIDEwMCkNCiAgICAgIH0NCiAgICB9KQ0KDQoNCiAgICB0aGlzLnBsYXRmb3JtLnJlYWR5KCkudGhlbigoKSA9PiB7DQogICAgICAvLyBkbyB3aGF0ZXZlciB5b3UgbmVlZCB0byBkbyBoZXJlLg0KICAgICAgc2V0VGltZW91dCgoKSA9PiB7DQogICAgICAgIHRoaXMuc3BsYXNoU2NyZWVuLmhpZGUoKQ0KICAgICAgfSwgMTAwKQ0KICAgIH0pDQoNCiAgfQ0KDQogIGxvZ2luKCkgew0KICAgIGlmICghdGhpcy5teUZvcm0udmFsaWQpew0KICAgICAgY29uc29sZS5sb2coJ0lOVkFMSUQnLCB0aGlzLm15Rm9ybS52YWx1ZSkNCiAgICAgIHJldHVybg0KICAgIH0NCg0KICAgIHZhciB2YWx1ZXMgPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLm15Rm9ybS52YWx1ZSkNCg0KICAgIGlmICh2YWx1ZXMpIHsNCiAgICAgIHRoaXMuc3RvcmFnZS5zZXQoJ2xvZ2luLWZvcm0nLCB0aGlzLm15Rm9ybS52YWx1ZSkNCiAgICAgIHRoaXMuc3RvcmFnZS5nZXQoJ2xvZ2luLWZvcm0nKS50aGVuKCh2YWwpID0+IHsNCiAgICAgICAgY29uc29sZS5sb2codmFsKQ0KICAgICAgfSkNCiAgICB9DQoNCiAgICBkZWxldGUgdmFsdWVzLnJlbWVtYmVyDQogICAgdGhpcy5hdXRoLmxvZ2luKHRoaXMubXlGb3JtLnZhbHVlKS5zdWJzY3JpYmUoZGF0YSA9PiB7DQogICAgICBpZiAoZGF0YSkgew0KICAgICAgICBjb25zb2xlLmxvZygnbG9naW4gJyxkYXRhKQ0KICAgICAgICBpZiAoIXRoaXMubXlGb3JtLnZhbHVlLnJlbWVtYmVyKSB0aGlzLnN0b3JhZ2UucmVtb3ZlKCdsb2dpbi1mb3JtJykNCiAgICAgICAgdGhpcy5zdG9yYWdlLnNldCgnb2ItYXBpLXRva2VuJywgZGF0YS50b2tlbikNCiAgICAgICAgdGhpcy50b2tlbnMub2JBcGlUb2tlbiA9IGRhdGEudG9rZW4NCiAgICAgICAgLy9lbWFpbCwgbWVzc2FnZSwgdG9rZW4NCiAgICAgICAgdGhpcy5uYXZDdHJsLnNldFJvb3QoSG9tZVBhZ2UpDQogICAgICAgIHJldHVybg0KICAgICAgfQ0KICAgICAgdGhpcy5zaG93RXJyb3IoKQ0KICAgIH0sIGVyciA9PiB7DQogICAgICB0aGlzLnNob3dFcnJvcigpDQogICAgICBjb25zb2xlLmVycm9yKGVycikNCiAgICB9KQ0KICB9DQoNCiAgcHJpdmF0ZSBzaG93RXJyb3IoKSB7DQogICAgY29uc3QgYWxlcnQgPSB0aGlzLmFsZXJ0Q3RybC5jcmVhdGUoew0KICAgICAgdGl0bGU6ICdFcnJvcicsDQogICAgICBzdWJUaXRsZTogJ1JldmlzZSBzdSBpbmZvcm1hY2nDs24geSB2dWVsdmEgaW50ZW50YXJsbycsDQogICAgICBidXR0b25zOiBbJ0FjZXB0YXInXQ0KICAgIH0pDQogICAgYWxlcnQucHJlc2VudCgpDQogIH0NCn0NCg0KDQoNCg0KDQoNCmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJw0KaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cEhlYWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCcNCmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzL09ic2VydmFibGUnDQppbXBvcnQgeyBDb25maWd1cmF0aW9uUHJvdmlkZXIgfSBmcm9tICcuLi8uLi9wcm92aWRlcnMvY29uZmlndXJhdGlvbic7DQoNCkBJbmplY3RhYmxlKCkNCmV4cG9ydCBjbGFzcyBBdXRoUHJvdmlkZXIgew0KDQogIHVybEJhc2U6IHN0cmluZyA9IENvbmZpZ3VyYXRpb25Qcm92aWRlci5yZXF1ZXN0VXJsKCkgKyAnLycNCiAgcHVibGljIHRva2VuOiBzdHJpbmcNCg0KICBjb25zdHJ1Y3RvcihwdWJsaWMgaHR0cDogSHR0cENsaWVudCkgew0KICAgIGNvbnNvbGUubG9nKCdIZWxsbyBBdXRoUHJvdmlkZXIgUHJvdmlkZXInKQ0KICB9DQoNCiAgbG9naW4oZGF0YSk6IE9ic2VydmFibGU8YW55PiB7DQogICAgbGV0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoeyAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nIH0pDQogICAgcmV0dXJuIHRoaXMuaHR0cA0KICAgICAgLmdldChgJHt0aGlzLnVybEJhc2V9P2VtYWlsPSR7ZGF0YS5lbWFpbH1gLCB7IGhlYWRlcnM6IGhlYWRlcnMgfSkNCiAgfQ0KfQ0KDQoNCg0KDQoNCg0KDQppbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZScNCmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBIZWFkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnDQppbXBvcnQgJ3J4anMvYWRkL29wZXJhdG9yL21hcCcNCmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzL09ic2VydmFibGUnDQppbXBvcnQgeyBDb25maWd1cmF0aW9uUHJvdmlkZXIgfSBmcm9tICcuLi8uLi9wcm92aWRlcnMvY29uZmlndXJhdGlvbic7DQppbXBvcnQgeyBUb2tlbnNQcm92aWRlciB9IGZyb20gJy4uL3Rva2Vucyc7DQoNCkBJbmplY3RhYmxlKCkNCmV4cG9ydCBjbGFzcyBQZXJzb25Qcm92aWRlciB7DQogIHVybEJhc2U6IHN0cmluZyA9IENvbmZpZ3VyYXRpb25Qcm92aWRlci5yZXF1ZXN0VXJsKCkgKyAnL2RhdGEnDQoNCiAgY29uc3RydWN0b3IocHVibGljIGh0dHA6IEh0dHBDbGllbnQsIHByaXZhdGUgdG9rZW5zOiBUb2tlbnNQcm92aWRlcikgew0KICB9DQoNCiAgcmVhZCgpOiBPYnNlcnZhYmxlPGFueT4gew0KICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0KGAke3RoaXMudXJsQmFzZX0/dG9rZW49JHt0aGlzLnRva2Vucy5vYkFwaVRva2VufWApDQogIH0gIA0KfQ0K"
     }

     loading.dismiss();
     //this.sendResults();
    }

    sendResults() {
      let loading = this.loadingCtrl.create({
        content: 'Sending results ...'
      });
      loading.present();

      this.result.send(this.json).subscribe(data => {
        loading.dismiss();
        if (data) {
          console.log(data)
          let profileModal = this.modalCtrl.create(ResultModalPage, data);
          profileModal.present();

          return
        }
      }, err => {
        console.error(err)
        let profileModal = this.modalCtrl.create(ResultModalPage, err);
        profileModal.present();
        loading.dismiss();
      })
    }






  // generate ID
   zfill(number, width) {
      var numberOutput = Math.abs(number); /* Valor absoluto del número */
      var length = number.toString().length; /* Largo del número */
      var zero = "0"; /* String de cero */

      if (width <= length) {
          if (number < 0) {
               return ("-" + numberOutput.toString());
          } else {
               return numberOutput.toString();
          }
      } else {
          if (number < 0) {
              return ("-" + (zero.repeat(width - length)) + numberOutput.toString());
          } else {
              return ((zero.repeat(width - length)) + numberOutput.toString());
          }
      }
  }
}
