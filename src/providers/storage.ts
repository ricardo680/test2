import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs/Observable'

//import { ConfigurationProvider } from '../../providers/configuration';
import { Storage } from '@ionic/storage';



@Injectable()
export class StorageProvider {

  public personsDb : Storage;


  //urlBase: string = ConfigurationProvider.requestUrl() + '/'
  public token: string

  constructor(public http: HttpClient) {
    this.personsDb = new Storage({
      name: '__my_persons_db',
      storeName: '_persons',
      driverOrder: ['sqlite', 'indexeddb', 'websql', 'localstorage']
    });
  }

}
