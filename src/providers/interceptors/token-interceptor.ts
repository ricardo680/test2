
import { Injectable } from '@angular/core'
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http'
import {Observable} from 'rxjs/Observable'

import { TokensProvider } from '../../providers/tokens';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private tokens: TokensProvider) { }



  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let _req = req.clone({
      setHeaders: {
        'authorization': `Bearer ${this.tokens.obApiToken}`
      }
    })
    return next
      .handle(_req)
      .do(
      data => {
        //console.log("t", data)
      },
      err => {
        if (err.status === 401 || err.status === 403) {
          console.error('you are not authenticated')
          return
        }
        //console.error(err)
      })
  }

}


///// https://www.youtube.com/watch?v=qnRrqH-BzJE
