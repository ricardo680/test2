import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import 'rxjs/add/operator/map'
import { Observable } from 'rxjs/Observable'

import { ConfigurationProvider } from '../../providers/configuration';

import { TokensProvider } from '../tokens';



@Injectable()
export class ResultProvider {

  urlBase: string = ConfigurationProvider.requestUrl() + '/result'

  constructor(public http: HttpClient, private tokens: TokensProvider) {
  }

  send(data): Observable<any> {
    return this.http.post(`${this.urlBase}?token=${this.tokens.obApiToken}`,data)
      //.catch((error: any) => Observable.throw(error || 'Server error'))
  }

}
