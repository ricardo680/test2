import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import 'rxjs/add/operator/map'
import { Observable } from 'rxjs/Observable'

import { ConfigurationProvider } from '../../providers/configuration';

import { TokensProvider } from '../tokens';



@Injectable()
export class PersonProvider {

  urlBase: string = ConfigurationProvider.requestUrl() + '/data'

  constructor(public http: HttpClient, private tokens: TokensProvider) {
  }

  read(): Observable<any> {
    return this.http.get(`${this.urlBase}?token=${this.tokens.obApiToken}`)
      //.catch((error: any) => Observable.throw(error || 'Server error'))
  }
  
}
