import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
//import 'rxjs/add/operator/map'
import { Observable } from 'rxjs/Observable'
//import 'rxjs/add/observable/throw'

import { ConfigurationProvider } from '../../providers/configuration';



@Injectable()
export class AuthProvider {

  urlBase: string = ConfigurationProvider.requestUrl() + '/'
  public token: string

  constructor(public http: HttpClient) {
    console.log('Hello AuthProvider Provider')
  }


  login(data): Observable<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' })
    return this.http
      .get(`${this.urlBase}?email=${data.email}`, { headers: headers })
  }

}
