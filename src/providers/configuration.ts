import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable()
export class ConfigurationProvider {

  static tester: Boolean = false;

  static productionRequestUrl: String = 'http://13.58.37.162:80'
  static testerRequestUrl: String = 'http://13.58.37.162:80'

  static requestUrl() {
    if (this.tester)
      return this.testerRequestUrl
    else
      return this.productionRequestUrl
  }

}
