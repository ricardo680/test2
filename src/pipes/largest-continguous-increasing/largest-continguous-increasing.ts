import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({
  name: 'largestContinguousIncreasing',
})
export class LargestContinguousIncreasingPipe implements PipeTransform {
  transform(values: any, ...args) {
    //debugger
    var largestArray = []
    var currentArray = []

    values.forEach(function(current, id) {
      //let current = values[id]
      if (id == "0") {
        currentArray.push(current)
        return
      }

      let last = values[id-1]
      if (last.age >= current.age) {
        if (currentArray.length > largestArray.length) {
          largestArray = currentArray
        }
        currentArray = []
      }

      currentArray.push(current)
    })

    return largestArray
  }


}
