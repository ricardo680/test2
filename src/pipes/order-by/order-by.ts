import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the OrderByPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'orderBy',
})
export class OrderByPipe implements PipeTransform {

  transform(records: Array<any>, args?: any) {
    debugger
    return records.sort(function(a, b){
      debugger
      if(a[args.property] < b[args.property]){
          return -1 * args.direction;
      }
      else if( a[args.property] > b[args.property]){
          return 1 * args.direction;
      }
      else{
          return 0;
      }
  });
  }
}
