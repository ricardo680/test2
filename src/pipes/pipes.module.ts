import { NgModule } from '@angular/core';
import { OrderByPipe } from './order-by/order-by';
import { LargestContinguousIncreasingPipe } from './largest-continguous-increasing/largest-continguous-increasing';
@NgModule({
	declarations: [OrderByPipe,
    LargestContinguousIncreasingPipe],
	imports: [],
	exports: [OrderByPipe,
    LargestContinguousIncreasingPipe]
})
export class PipesModule {}
